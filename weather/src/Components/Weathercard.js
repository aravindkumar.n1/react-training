import React from "react";
import moment from "moment";
import { Button } from "semantic-ui-react";
import { Table } from "semantic-ui-react";

const Weathercard = (props) => {
  const reload = () => {
    window.location.reload();
  };
  return (
    <div className="main">
      <div className="top">
        <p className="header">City name : {props.weatherData.location.name}</p>
        <Button
          className="button"
          inverted
          color="blue"
          circular
          icon="refresh"
          onClick={reload}
        />
      </div>
      <div className="flex">
        <span className="day">Day: {moment().format("dddd")}</span>
        <span className="description">
          {props.weatherData.forecast.forecastday[0].date}
        </span>
      </div>
      <div className="flex">
        <p className="temp">
          Temprature: {props.weatherData.current.temp_c}&deg;C
        </p>
        <span className="temp">
          Humidity : {props.weatherData.current.humidity}
        </span>
      </div>
      <div className="flex">
        <span className="sunrise-sunset">
          Sunrise : {props.weatherData.forecast.forecastday[0].astro.sunrise}
        </span>
        <span className="sunrise-sunset">
          Sunset : {props.weatherData.forecast.forecastday[0].astro.sunset}
        </span>
      </div>

      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Day</Table.HeaderCell>
            <Table.HeaderCell>Avg Temprature </Table.HeaderCell>
            <Table.HeaderCell>Avg Humidity</Table.HeaderCell>
            <Table.HeaderCell>Sunrise</Table.HeaderCell>
            <Table.HeaderCell>Sunset</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {props.weatherData.forecast.forecastday.map((value, index) => {
            console.log(value);
            return (
              <Table.Row key={index}>
                <Table.Cell>{value.date}</Table.Cell>
                <Table.Cell>{value.day.avgtemp_c}</Table.Cell>
                <Table.Cell>{value.day.avghumidity}</Table.Cell>
                <Table.Cell>{value.astro.sunrise}</Table.Cell>
                <Table.Cell>{value.astro.sunset}</Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    </div>
  );
};

export default Weathercard;
