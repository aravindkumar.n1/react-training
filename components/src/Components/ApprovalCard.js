import React from "react";
import { connect } from "react-redux";
import CommentDetail from "./CommentDetails";
import { startTimer } from "../Actions/actionCreation";

class ApprovalCard extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.startTimer();
  }
  render() {
    return (
      <div className="ui card">
        <div>{this.props.children}</div>
        <p> &nbsp;&nbsp;&nbsp;&nbsp; Current time is : {this.props.time}</p>
        <div className="extra content">
          <div className="ui two buttons">
            <div className="ui basic green button">Approve</div>
            <div className="ui basic red button">Reject</div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    time: state.time,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    startTimer: () => {
      dispatch(startTimer());
    },
  };
};

ApprovalCard = connect(mapStateToProps, mapDispatchToProps)(ApprovalCard);

export default ApprovalCard;
