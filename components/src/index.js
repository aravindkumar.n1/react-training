import React, { useEffect } from "react";
import ReactDom from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import CommentDetail from "./Components/CommentDetails";
import rootReducer from "./Reducers/rootReducers";
import ApprovalCard from "./Components/ApprovalCard";
const App = () => {
  useEffect(() => {});
  return (
    <div>
      <ApprovalCard>
        <CommentDetail
          author="Deva"
          comment="nicely done bro"
          time="10.15 am"
        />
        <CommentDetail
          author="Buchharao"
          comment="nicely done bro"
          time="10.15 am"
        />
      </ApprovalCard>
    </div>
  );
};

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

ReactDom.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
