import React from "react";

const Mainnav = () => {
  return (
    <nav className="navbar navbar-inverse">
      <div className="container-fluid">
        <div className="navbar-header">
          <button
            type="button"
            className="navbar-toggle"
            data-toggle="collapse"
            data-target="#myNavbar"
          >
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <span className="navbar-brand">Happy Learning</span>
        </div>
        <div className="collapse navbar-collapse" id="myNavbar">
          <ul className="nav navbar-nav">
            <li className="active">
              <span>Home</span>
            </li>
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li>
              <span className="glyphicon glyphicon-log-in"></span> Login
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Mainnav;
