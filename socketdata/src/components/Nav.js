import React from "react";
import { Link } from "react-router-dom";

const Nav = () => {
  return (
    <div className="col-sm-4 sidenav " style={{ paddingTop: "10px" }}>
      <p>
        <Link to="/">Home</Link>
      </p>
      <p>
        <Link to="/redux">Current Time</Link>
      </p>
      <p>
        <Link to="/airquality">Airquality</Link>
      </p>
      <p>
        <Link to="/weather">Weather Data</Link>
      </p>
    </div>
  );
};

export default Nav;
