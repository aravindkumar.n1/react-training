import React from "react";
import { connect } from "react-redux";
import { startTimer } from "../Actions/actionCreation";
import faker from "faker";
import CommentDetail from "./CommentDetails";

class ApprovalCard extends React.Component {
  //   constructor(props) {
  //     super(props);
  //   }

  componentDidMount() {
    this.props.startTimer();
  }
  render() {
    return (
      <div class="ui card">
        <div class="content">
          <img
            alt="User Avatar here"
            class="right floated mini ui image"
            src={faker.image.avatar()}
          />
          <div class="header">Elliot Fu</div>
          <div class="meta">Friends of Veronika</div>
          <div class="description">
            <CommentDetail time={this.props.time} />
          </div>
        </div>
        <div class="extra content">
          <div class="ui two buttons">
            <div class="ui basic green button">Approve</div>
            <div class="ui basic red button">Decline</div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    time: state.time,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    startTimer: () => {
      dispatch(startTimer());
    },
  };
};

ApprovalCard = connect(mapStateToProps, mapDispatchToProps)(ApprovalCard);

export default ApprovalCard;
